# Attività 1 - MongoDB

### Descrizione

In questa repository sono presenti i file per la consegna dell'attività di Big Data su MongoDB. Ho scelto di modellare dati di tipo finanziario, creando due file CSV con dati scaricati da fonti diverse. Ho trasformato poi questi file in JSON e ho caricato i dati all'interno di un'istanza di MongoDB. Ho creato due notebook jupyter, il primo per scaricare e modellare i dati in formato CSV, il secondo per trasformare questi file CSV in formato JSON.
Il PDF "BDA - Attività MongoDB" contiene la descrizione dell'intero processo di modellazione e alcune query d'esempio.

### Notebooks

All'interno della cartella "notebooks" è presente il file ('Attività 1 - Download e adattamento dataset.ipynb') per il download dei dati in formato CSV. Scaricare tutti i dati comporta una notevole quantità di tempo (nell'ordine delle ore), consiglio quindi di utilizzare direttamente i file CSV in caso si desideri testare il secondo notebook. 
Il secondo notebook ('Attività 1 - Trasformazione in JSON.ipynb') trasforma i file CSV in file JSON e li salva all'interno della cartella "JSON".
I risultati dei notebooks sono visibili nei file .html che ho caricato, evitando così di dover installare i requirements (presenti nel file "requirements.txt").
Se si vuole comunque testare il codice consiglio di creare un virtual environment ed installare i requirements utilizzando il comando:
`pip install -r requirements.txt`

### Caricamento delle collezioni in MongoDB

Per poter testare le collezioni create si devono caricare in un'istanza di MongoDB i file JSON. Per fare ciò è necessario seguire alcuni passi:
* Avviare il server MongoDB
* Collocarsi nella cartella "JSON"
* Eseguire il comando `mongoimport --db test_db meta.json` per caricare nel database di test i documenti della collezione "meta"
* Eseguire il comando `mongoimport --db test_db stocks.json` per caricare i documenti della collezione "stocks"

Si possono quindi testare le query presenti nel PDF di descrizione.